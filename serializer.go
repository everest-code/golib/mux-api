package api

import (
	"errors"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// BindRequest bind JSON request into val
func BindRequest(req *http.Request, val interface{}) *Error {
	if req.Body == nil {
		return FromError(errors.New("request body is nil or empty"))
	}

	data, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return FromError(err)
	}

	return FromError(json.Unmarshal(data, val))
}

// WriteResponse write the payload in JSON into the response
func WriteResponse(res http.ResponseWriter, statusCode int, val interface{}) {
	res.Header().Add("Content-Type", "application/json")
	res.WriteHeader(statusCode)
	data, _ := json.Marshal(val)
	res.Write(data)
}
