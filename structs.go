package api

import (
	"fmt"
	"reflect"
	"strings"
)

type (
	Response struct {
		Ok      bool        `json:"ok"`
		Payload interface{} `json:"payload"`
		Error   *Error      `json:"error,omitempty"`
	}

	Error struct {
		Name        string      `json:"name"`
		Description interface{} `json:"description"`
	}
)

// NewOkResponse create a new HTTP Response from a payload,
// Note: Only OK responses
func NewOkResponse(payload interface{}) *Response {
	return &Response{
		Ok:      true,
		Payload: payload,
		Error:   nil,
	}
}

// NewErrorResponse create a new HTTP Response with an error and without payload,
// Note: Only for error responses
func NewErrorResponse(err *Error) *Response {
	return &Response{
		Ok:      false,
		Payload: nil,
		Error:   err,
	}
}

// FromError Create an api.Error from a go error
func FromError(err error) *Error {
	if err == nil {
		return nil
	}
	name := strings.ReplaceAll(reflect.TypeOf(err).String(), "*", "")
	if name == "errors.errorString" {
		name = "runtimeError"
	}

	return &Error{
		Name:        name,
		Description: err.Error(),
	}
}

func (e *Error) Error() string {
	if e.Description == nil {
		return e.Name
	}

	return fmt.Sprintf("[%s]: %#v", e.Name, e.Description)
}
